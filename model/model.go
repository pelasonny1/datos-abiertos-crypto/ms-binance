package model

type OrderBookEntry struct {
	Price  string `json:"price"`
	Amount string `json:"amount"`
}

type OrderBook struct {
	LastUpdateID int              `json:"lastUpdateId"`
	Bids         []OrderBookEntry `json:"bids"` // Órdenes de compra
	Asks         []OrderBookEntry `json:"asks"` // Órdenes de venta
}
