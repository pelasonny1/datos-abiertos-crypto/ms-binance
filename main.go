package main

import (
	"Binance/client"
	"encoding/json"
	"log"
	"net/http"
)

func main() {
	cli := client.NewClient()

	http.HandleFunc("/precio", func(w http.ResponseWriter, r *http.Request) {
		symbols := []string{"BTCUSDT", "ETHUSDT", "BNBUSDT"}
		precios, err := cli.GetPrices(symbols)
		if err != nil {
			http.Error(w, "Error al obtener los precios: "+err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(precios)
	})

	http.HandleFunc("/orderbook", func(w http.ResponseWriter, r *http.Request) {
		symbol := r.URL.Query().Get("symbol")
		if symbol == "" {
			http.Error(w, "Símbolo es requerido", http.StatusBadRequest)
			return
		}

		orderBook, err := cli.GetOrderBook(symbol)
		if err != nil {
			http.Error(w, "Error al obtener el libro de órdenes: "+err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(orderBook)
	})

	log.Println("Servidor iniciado en http://localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
