package client

import (
	"Binance/model"
	"encoding/json"
	"net/http"
	"os"
	"strconv"
)

const baseURL = "https://api.binance.com"

type Client struct {
	httpClient *http.Client
	APIKey     string
	APISecret  string
}

func NewClient() *Client {
	return &Client{
		httpClient: &http.Client{},
		APIKey:     os.Getenv("j4VBZJFQv2cXWaOOHcwXuOiGk8O6v22VCd0GvPslZW73LReQKpcgYvDmyatrwvNj"),
		APISecret:  os.Getenv("ExC8VM9Y8BaTe2aDwmPCz1v3zHrRVJtLzN64IajDnWIttNx9wNhBczmoGSXcRBR5"),
	}
}

var criptomonedas = []string{"BTCUSDT", "ETHUSDT", "BNBUSDT"}

func (c *Client) GetPrice(symbol string) (float64, error) {
	url := baseURL + "/api/v3/ticker/price?symbol=" + symbol
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return 0, err
	}

	req.Header.Add("X-MBX-APIKEY", c.APIKey)

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	var result struct {
		Price string `json:"price"`
	}
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return 0, err
	}

	price, err := strconv.ParseFloat(result.Price, 64)
	if err != nil {
		return 0, err
	}

	return price, nil
}

func (c *Client) GetPrices(symbols []string) (map[string]float64, error) {
	precios := make(map[string]float64)
	for _, symbol := range symbols {
		precio, err := c.GetPrice(symbol)
		if err != nil {
			return nil, err
		}
		precios[symbol] = precio
	}
	return precios, nil
}

func (c *Client) GetOrderBook(symbol string) (*model.OrderBook, error) {
	url := baseURL + "/api/v3/depth?symbol=" + symbol + "&limit=100"

	resp, err := c.httpClient.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var orderBook model.OrderBook
	if err := json.NewDecoder(resp.Body).Decode(&orderBook); err != nil {
		return nil, err
	}

	return &orderBook, nil
}
